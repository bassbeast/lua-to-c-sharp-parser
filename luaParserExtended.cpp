#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <iomanip>
#include <vector>
#include <map>

using namespace std;

bool containsChar(string s, char c)
{
    for(int i=0; i<s.length(); i++)
    {
        if(s[i] == c)
        {
            return true;
        }
     }
}

bool containsDigit(string s)
{
    for(int i=0; i<s.length(); i++)
    {
        if(isdigit(s[i]))
        {
            return true;
        }
    }
    return false;
}

bool containsAlpha(string s)
{
    for(int i=0; i<s.length(); i++)
    {
        if(isalpha(s[i]))
        {
            return true;
        }
    }
    return false;
}

int count(string s, char c)
{
    int count = 0;
    for(int i=0; i<s.length(); i++)
    {
        if(s[i] == c)
        {
            count++;
        }
    }
    return count;
}

string deleteWS(string s)
{
    string q;
    for(int i=0; i<s.length(); i++)
    {
        if(s[i] != ' ' )
        {
            q += s[i];
        }
    }
    return q;
}

bool strSort(string a, string b)
{
    return a < b;
}

string indent(int i)
{
    string s;
    for(int j=0; j<i*4; j++)
    {
        s += ' ';
    }
    return s;
}

int getNthCharacter(string s, char c, int i)
{
    int count = 0;
    for(int j=0; j<s.length(); j++)
    {
        if(s[j] == c)
        {
            count++;
            if(count == i)
            {
                return j;
            }
        }
    }
    return -1;
}
class func
{
    public:
        map<string, string> locVar;
        vector<string> logic;
};
int main()
{
    string s;
    fstream in, out;
    while(getline(cin, s))
    {
        int indentation = 1;
        in.open(s);
        string filename = s.substr(0, s.find("lua")-1);
        out.open(s + ".cs", fstream::out);
        vector<string> var;
        map<string, func> functions;
        out << "namespace Dawngate.Abilities {\n";
        if(count(s, '_') == 4)
        {
            out << indent(1) << "public class "
            << s.substr(s.find('_')+1, (getNthCharacter(s, '_', 3)-1)-s.find('_')) << endl
            << indent(2) << "public class "
            << s.substr((getNthCharacter(s, '_', 3)+1), (s.length()-4)-(getNthCharacter(s, '_', 3)+1))
            << endl;
            indentation = 2;
        }
        if(count(s, '_') == 3)
        {
            out << indent(1) << "public class "
            << s.substr(s.find('_')+1, getNthCharacter(s, '_', 2)-s.find('_')-1)
            << " {" << endl
            << indent(2) << "public class "
            << s.substr(getNthCharacter(s, '_', 2)+1, (s.length()-4)-(getNthCharacter(s, '_', 2)+1))
            << " {" << endl;
            indentation = 3;
        }
        else if(count(s, '_') == 2)
        {
            out << indent(1) << "public class "
            << s.substr(1+s.find('_'), s.rfind('_') - s.find('_')-1) << " {" <<  endl
            << indent(2) <<  "public class "
            << s.substr(s.rfind('_')+1, s.length() - s.rfind('_')-5) << " {" << endl;
            indentation = 3;
        }
        else if(count(s, '_') == 1)
        {
            out << indent(1) << "public class "
            << s.substr(s.find('_')+1, (s.length()-4)-s.find('_'));
            indentation = 2;
        }
        while(getline(in, s))
        {
            if(s.length() > 0)
            {
                s.erase(s.length()-1);
            }
            if(count(s, '=') == 1 && s[0] != ' ')//Global Variables/Functions
            {
                if(containsAlpha(s.substr(s.find('='), s.length()-s.find('='))))//type variables
                {
                    if(s.substr(s.find('='), s.length()-s.find('=')).find("true") != -1
                    || s.substr(s.find('='), s.length()-s.find('=')).find("false") != -1)//booleans
                    {
                        int k = count(s.substr(0, s.find('=')), '_');
                        if(s.substr(0, s.find('=')).find('.') == -1)
                        {
                            var.push_back(indent(indentation) + "public bool "
                            + s.substr(getNthCharacter(s, '_', k)+1, s.length()-(k+1)) + ';');
                        }
                        else
                        {
                            var.push_back(indent(indentation) + "public bool "
                            + s.substr(s.find('.')+1, s.length()-(s.find('.')+1)) + ';');
                        }
                    }
                    else if(s.substr(s.find('='),  s.length()-s.find('=')).find("Bit") != -1)//Enum
                    {
                        if(s.substr(0, s.find('=')).find('.') != -1)
                        {
                            string q = s.substr(s.find('.')+1, s.find('=')-(s.find('.')+1));
                            q[0] = toupper(q[0]);
                            string p = s.substr(s.find('(')+1, s.find(')')-(s.find('(')+1));
                            string m = "";
                            for(int i=0; i<p.length();i++)
                            {
                                if(p[i] == ',')
                                {
                                    m += " |";
                                }
                                else
                                    m += p[i];
                            }
                            var.push_back(indent(indentation) + "public "
                            + q + s.substr(s.find('.')+1, s.find('=')-(s.find('.')-1))
                            + m + ';');
                        }
                        else
                        {
                            //var.push_back();
                        }
                    }
                    /*else if(s.substr(s.find('=')+1, s.length()-(s.find('=')+1)).find('(') != -1
                    && s.find('[') == -1)
                    {
                        if(s.substr(0, s.find('=')).find('.') != -1)
                        {
                            int i = s.substr(s.find('=')+1, s.find('(')-(s.find('=')+1)).rfind('.')+1
                            + (s.find('=')+1);
                            string q = s.substr(i, s.find('(')-i);
                            q[0] = toupper(q[0]);
                            string p = s.substr(s.find('.')+1, s.find('=')-(s.find('.')+1));
                            string m = s.substr(s.find('='), s.length()-(s.find('=')));
                            var.push_back(indent(indentation) + "public " + q + ' ' + p + m + ';');
                        }
                        else
                        {
                            var.push_back(indent(indentation)+ '/' + '/' + "public "
                            + s.substr(s.substr(0, s.find('=')).rfind('_')+1,
                            s.find('=')-(s.substr(0, s.find('=')).rfind('_')+1))
                            + (char)tolower(s[s.substr(0, s.find('=')).rfind('_')+1]) + ' '
                            + s.substr(s.find('='), s.length()-s.find('=')) + ';');
                        }
                    }*/
                    else
                    {
                        if(s.substr(0, s.find('=')).find('.') != -1
                        && s.substr(0, s.find('=')).find('[') == -1)
                        {
                            string q = s.substr(s.find('.')+1, s.find('=')-(s.find('.')+1));
                            q[0] = toupper(q[0]);
                            var.push_back(indent(indentation) + "public " + q
                            + s.substr(s.find('.')+1, s.length()-(s.find('.')+1)) + ';');
                        }
                        else
                        {
                            if(s.substr(0, s.find('=')).find('_') != -1
                            && s.find("function") != -1)//Functions
                            {
                                string key = indent(indentation) + "public void "
                                + s.substr(s.find('.')+1, s.find(']')-(s.find('.')+1))
                                + s.substr(s.find('('), s.find(')')+1-s.find('('))
                                + '{';
                                func f;
                                functions.insert(pair<string, func>(key, f));
                                while(s.find("end") == string::npos
                                || (!s[s.find("end")-1] == ' ' || !s.find("end") == 0)
                                && !in.eof())
                                {
                                    getline(in, s);
                                    int i=0;
                                    while(s[i] == ' ')//indentation
                                    {
                                        i++;
                                    }
                                    if(s.find(filename) != -1)
                                    {
                                        if(s.find('L') != -1
                                        && s.find('=') != -1)
                                        {
                                            functions[key].locVar.insert(pair<string, string>
                                            (s.substr(s.find('L'), s.find('=')-s.find('L')-1),
                                            "this"));
                                            s = "";
                                        }
                                    }
                                    if(s.find("L") != string::npos
                                    && s[s.find("L")-1] == ' ')
                                    {
                                        if(s.find('=') != string::npos)
                                        {
                                            
                                        }
                                    }
                                    map<string, string>::iterator it = functions[key].locVar.begin();
                                    if(s.length() > 0)
                                    {
                                        while(it != functions[key].locVar.end())
                                        {
                                            if(s.find(it->first) != -1)
                                            {
                                                s.replace(s.find(it->first),
                                                it->first.length(), it->second);
                                            }
                                            it++;
                                        }
                                        if(s.find("then") != -1
                                        && s[s.find("then")-1] == ' ')
                                        {
                                            s.replace(s.find("then"), 4, "{");
                                        }
                                        if(s.find("end") != string::npos
                                        && (s[s.find("end")-1] == ' ' || s.find("end") == 0))
                                        {
                                            s.replace(s.find("end"), 3, "}");
                                        }
                                        if(s.find("else") != -1)
                                        {
                                            if(functions[key].logic[functions[key].logic.size()-1] != "}")
                                            {
                                                functions[key].logic.push_back(indent(indentation+(i/2)) + '}');
                                            }
                                            if(s.length() == i+4)
                                            {
                                                s += " {";
                                            }
                                        }
                                        if(s.find("if") != -1
                                        && s.find("else") == -1
                                        && s[s.find("if")-1] == ' ')
                                        {
                                            s.replace(s.find("if"), 2, "if (");
                                            if(s.find("{") != -1)
                                            {
                                                s.replace(s.find("{"), 1, ") {");
                                            }
                                        }
                                        if(s.find("elseif(") != -1)
                                        {
                                            s.replace(s.find("elseif"), 6, "else if (");
                                            if(s.find("{") != -1)
                                            {
                                                s.replace(s.find("{"), 1, ") {");
                                            }
                                        }
                                        if(s.find("not") != -1)
                                        {
                                            s.replace(s.find("not"), 3, "!");
                                        }
                                        if(s.find("and") != -1)
                                        {
                                            s.replace(s.find("and"), 3, "&&");
                                        }
                                        if(s.find("or") != -1
                                        && s.find("or")-1 == ' ')
                                        {
                                            s.replace(s.find("or"), 2, "||");
                                        }
                                        if(s.find("local") == string::npos)
                                        {
                                            functions[key].logic.push_back(indent(indentation+(i/2))
                                            + s.substr(i, s.length()-i));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var.push_back(indent(indentation) + '/' + '/' + s + ';');
                            }
                        }
                    }
                }//#End type variables
                else if(count(s, '{') == 1
                && !containsAlpha(s.substr(s.find('=')+1, s.length()-(s.find('=')+1))))//Number arrays
                {
                    if(!containsDigit(s.substr(s.find('=')+1, s.length()-(s.find('=')+1))))
                    {
                        if(s.substr(0, s.find('=')).find('.') != -1)
                        {
                            string q = indent(indentation) + "public float[] "
                            + s.substr(s.find('.')+1, s.find('=')-s.find('.')) + " new float[]"
                            + s .substr(s.find('=')+1, s.length()-(s.find('=')+1));
                            while(!containsChar(s, '}'))
                            {
                                getline(in, s);
                                if(s.length() > 0)
                                {
                                    s.erase(s.length()-1);
                                }
                                q += (deleteWS(s));
                            }
                            q += ";";
                            if(q.substr(q.find('='), q.length()-q.find('=')).find('.') == -1)
                            {
                                q.replace(q.find("float[]"), 7, "int[]");
                                q.replace(q.find("float[]"), 7, "int[]");
                            }
                            var.push_back(q);
                        }
                        else
                        {
                            string q = indent(indentation) + "public float[] "
                            + s.substr(s.substr(0, s.find('=')).rfind('_')+1,
                            s.find('=')-(s.substr(0, s.find('=')).rfind('_')+1))
                            + " = new float[]" + s.substr(s.substr(0, s.find('=')).rfind('_')+1,
                            s.find('=')-(s.substr(0, s.find('=')).rfind('_')+1));
                            while(!containsChar(s, '}'))
                            {
                                getline(in, s);
                                if(s.length() > 0)
                                {
                                    s.erase(s.length()-1);
                                }
                                q += deleteWS(s);
                            }
                            q += ';';
                            if(q.substr(q.find('='), q.length()-q.find('=')).find('.') != -1)//int array
                            {
                                q.replace(q.find("float[]"), 7, "int[]" );
                            }
                            var.push_back(q);
                        }
                    }
                    else
                    {
                        int k = count(s.substr(0, s.find('=')), '_');
                        if(s.substr(0, s.find('=')).find('.') == -1)
                        {
                            if(s.substr(s.find('='), s.length()-s.find('=')).find('.') == -1)//int array
                            {
                                var.push_back(indent(indentation) + "public int[] "
                                + s.substr(getNthCharacter(s, '_', k)+1,
                                s.length()-getNthCharacter(s, '_', k)) + ";");
                            }
                            else//float array
                            {
                                var.push_back(indent(indentation) + "public float[] "
                                + s.substr(getNthCharacter(s, '_', k)+1,
                                s.length()-getNthCharacter(s, '_', k)) + ';');
                            }
                        }
                        else
                        {
                            if(s.substr(s.find('='), s.length()-s.find('=')).find('.') == -1)
                            {
                                    var.push_back(indent(indentation) + "public int[] "
                                    + s.substr(s.find('.')+1, s.length()-s.find('.')) + ';');
                            }
                            else
                            {
                                var.push_back(indent(indentation) + "public float[] "
                                + s.substr(s.find('.')+1, s.length()-(s.find('.')+1)) + ';');
                            }
                        }
                    }
                }//#End Array
                else//Not an array
                {
                    int k = count(s.substr(0, s.find('=')), '_');
                    if(s.substr(0, s.find('=')).find('.') == -1)
                    {
                        if(s.substr(s.find('='), s.length()-s.find('=')).find('.') == -1)
                        {
                            var.push_back(indent(indentation) + "public int "
                            + s.substr(getNthCharacter(s, '_', k)+1,
                            s.length()-getNthCharacter(s, '_', k)+1) + ';');
                        }
                        else
                        {
                            var.push_back(indent(indentation) + "public float "
                            + s.substr(getNthCharacter(s, '_', k)+1,
                            s.length()-getNthCharacter(s, '_', k)+1) + "f;");
                        }
                    }
                    else
                    {
                        if(s.substr(s.find('='), s.length()-s.find('=')).find('.') == -1)
                        {
                            var.push_back(indent(indentation) + "public int "
                            + s.substr(s.find('.')+1, s.length()-s.find('.')) + ';');
                        }
                        else
                        {
                            var.push_back(indent(indentation) + "public float "
                            + s.substr(s.find('.')+1, s.length()-s.find('.')) + "f;");
                        }
                    }
                }
            }//#end variabes
            else//logic
            {
               /* int i=0;
                while(s[i] == ' ')//indentation
                {
                    i++;
                }
                if(s.find(filename) != -1)
                {
                    if(s.find('L') != -1
                    && s.find('=') != -1)
                    {
                        locvar.insert(pair<string, string>(s.substr(s.find('L'), s.find('=')-s.find('L')-1),
                        "this"));
                        s = "";
                    }
                }
                map<string, string>::iterator it = locvar.begin();
                if(s.length() > 0)
                {
                    while(it != locvar.end())
                    {
                        if(s.find(it->first) != -1)
                        {
                            s.replace(s.find(it->first), it->first.length(), it->second);
                        }
                        it++;
                    }
                    if(s.find("then") != -1
                    && s[s.find("then")-1] == ' ')
                    {
                        s.replace(s.find("then"), 4, "{");
                    }
                    if(s.find("end") != -1
                    && (s[s.find("end")-1] == ' ' || s.length() == 3))
                    {
                        s.replace(s.find("end"), 3, "}");
                    }
                    if(s.find("else") != -1)
                    {
                        if(log[log.size()-1] != "}")
                        {
                            log.push_back(indent(indentation+(i/2)-1) + '}');
                        }
                        if(s.length() == i+4)
                        {
                            s += " {";
                        }
                    }
                    if(s.find("if") != -1
                    && s.find("else") == -1
                    && s[s.find("if")-1] == ' ')
                    {
                        s.replace(s.find("if"), 2, "if (");
                        if(s.find("{") != -1)
                        {
                            s.replace (s.find("{"), 1, ") {");
                        }
                    }
                    if(s.find("elseif") != -1)
                    {
                        s.replace(s.find("elseif"), 6, "else if (");
                        if(s.find("{") != -1)
                        {
                            s.replace(s.find("{"), 1, "){");
                        }
                    }
                    if(s.find("not") != -1)
                    {
                        s.replace(s.find("not"), 3, "!");
                    }
                    if(s.find("and") != -1)
                    {
                        s.replace(s.find("and"), 3, "&&");
                    }
                    if(s.find("or") != -1
                    && s.find("or")-1 == ' ')
                    {
                        s.replace(s.find("or"), 2, "||");
                    }
                    if(s.find("local") != -1)
                    {

                    }
                    else
                    {
                        log.push_back(indent(indentation+(i/2)-1) + s.substr(i, s.length()-i));
                    }
                }*/
            }//#end logic
        }
        //log.push_back("*/");
        //log.insert(log.begin(), "/*");
        /*if(indentation > 1)
        {
            while(indentation > 0)
            {
                log.push_back(indent(indentation-1) + "}");
                indentation--;
            }
        }
        else
        {
            log.push_back("}");
        }*/
        sort(var.begin(), var.end());
        for(int i=0; i<var.size(); i++)
        {
            out << var[i] << endl;
        }
        map<string, func>::iterator i = functions.begin();
        while(i != functions.end())
        {
            out << i->first << endl;
            map<string, string>::iterator j = i->second.locVar.begin();
            /*while(j != i->second.locVar.end())
            {
                out << j->second << endl;
                j++;
            }*/
            vector<string>::iterator k = i->second.logic.begin();
            while(k != i->second.logic.end())
            {
                out << *k << endl;
                k++;
            }
            i++;
        }
        /*for(int i=0; i<log.size(); i++)
        {
            out << log[i] << endl;
        }*/
        //locvar.clear();
        var.clear();
        out.close();
        out.clear();
        in.close();
        in.clear();
    }
}
